const express = require('express')
const app = express()
const port = process.env.PORT || 3000;
const mongoose = require('mongoose');
const bodyParser = require('body-parser')

app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json())

// app.use(express.json());
// app.use(express.urlencoded({extended: false}));

const devDbUrl = 'mongodb://localhost/new';
const mongoDB = process.env.MONGODB_URI || devDbUrl
mongoose.connect(mongoDB,
    {
        useCreateIndex: true,
        useFindAndModify: false,
        useNewUrlParser: true
    });
mongoose.Promise = global.Promise;
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

const user = require ('./routes/user')
app.use('/api/v2/user', user)

app.listen(port, () => console.log(`Example app listening on port ${port}!`))