let User = require ('../models/user')
let bcrypt = require('bcryptjs')

module.exports = {
    generateToken (req, res) {
        User.findOne ({email: req.body.email})
        .then (user => {
           var password = bcrypt.compareSync(req.body.password, user.password)
           if (user.email == req.body.email && password) {
            var jwt = require('jsonwebtoken');
            var token = jwt.sign({_id: user._id}, 'shhhhh');
            res.send({
                success: true,
                token: token
            })
            } else {
            res.send('Username or Password is wrong')
            }
        })
        .catch (user => {
            res.json({
                'error': user,
                'message': "email doesn't exist" 
            })
        })
    },
    loginMessage (req,res,next) {
        res.send ('Welcome')
        next()
    },
    currentUser (req, res){
        User.findOne ({_id: req.user._id})
        .then (user => {
           res.send({
            fullName: user.fullName,
            email: user.email
           })
        })
    }
}