let Transaction = require ('../models/transaction')
const {successResponse, errorResponse} = require ('../helpers/responseFormatter')
let User = require ('../models/user')

module.exports = {
    createTransaction (req, res) {
        let _id = req.user._id
        let trx = req.body.trx
        User.findOne({_id: _id}, function(err,user){
            if (!err){
                trx = new Transaction ({
                    trxInvoiceNumber: req.body.trxInvoiceNumber,
                    invoiceDate: req.body.invoiceDate,
                    noRekening: req.body.noRekening,
                    trxLotAmount: req.body.trxLotAmount,
                    trxReceiptStatus: req.body.trxReceiptStatus,
                    trxPaymentStatus: req.body.trxPaymentStatus,
                    productName: req.body.productName
                });
                trx.save (function(err){
                    if (err) {
                        throw err
                    }
                })
                user.transactions.push(trx._id)
                user.save(function(err) {
                    if (err) {
                        throw err;
                    };
                    res.status(201).json({
                        success: true, message: "Purchase successfull!"
                    })
                })
            } else {
                console.log (err);
                throw err;
            }
        })
    }
}