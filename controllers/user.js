let User = require ('../models/user')
let encryptor = require ('../helpers/bcrypt')
const {successResponse, errorResponse} = require ('../helpers/responseFormatter')
let bcrypt = require('bcryptjs')
let Transaction = require ('../models/transaction')

module.exports = {
    createUser (req,res) {
        User.create({
            email: req.body.email,
            fullName: req.body.fullName,
            password: encryptor(req.body.password)
        }, function (err, user){
            if (err) {
                res.status(422).json(errorResponse(err))
            } else{
                res.status(201).json(successResponse(user))
            }
        })
    },
    updateUser (req, res) {
        User.updateOne ({email: req.body.email}, {
            fullName: req.body.fullName,
            password: encryptor(req.body.password)
        })
        .then (post => {
            res.status(200).json(successResponse(post))
        })
        .catch (err => {
            res.status(422).json(errorResponse(err))
        })
    },
    trxbyUser(req, res){
        User.findOne({fullName: req.params.fullName})
        .populate ({path: 'transactions'})
        .exec((err, transactions)=> {
            res.send(successResponse(transactions.transactions))
            })
    }
}