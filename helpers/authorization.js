const jwt = require ('jsonwebtoken')


module.exports = function (req, res, next){
    var isUser = req.headers.authorization
    if (!isUser) {
        res.status(403).json({
            access: 'Denied'
        })
    }
    var decoded = jwt.verify(isUser, 'shhhhh')
        req.user = decoded
        next();
}