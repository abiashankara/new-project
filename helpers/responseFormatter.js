module.exports = {
    successResponse(data) {
        return{
            success: true,
            result: data
        }
    },
    errorResponse(err) {
        return {
            success: false,
            error: err
        }
    }
}