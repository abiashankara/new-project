const mongoose = require ('mongoose')
const Schema = mongoose.Schema
const userSchema = new mongoose.Schema ({
    fullName: {
        type: String,
        required: true,
        max: 100
    },
    email: {
        type: String,
        trim: true,
        lowercase: true,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true,
        min: 6,
        max: 20
    },
    transactions: [{
        type: Schema.Types.ObjectId,
        ref: 'Transaction'
    }]
})

module.exports = mongoose.model('User', userSchema);