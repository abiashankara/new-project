const mongoose = require ('mongoose')
const Schema = mongoose.Schema

const transactionSchema = new mongoose.Schema ({
    trxInvoiceNumber: {
        type: String,
        required: true
    },
    invoiceDate: {
        type: Date,
        required: true
    },
    noRekening: {
        type: String,
        required: true
    },
    trxLotAmount: {
        type: String,
        required: true
    },
    trxReceiptStatus: {
        type: Boolean,
        required: true
    },
    trxPaymentStatus: {
        type: Boolean,
        required: true
    },
    userId: {type: Schema.Types.ObjectId, ref: 'User'},
    productName: {
        type: String,
        required: true
    }
})


module.exports = mongoose.model('Transaction', transactionSchema);