var express = require('express')
var router = express.Router()
let userController = require ('../controllers/user')
let loginController = require ('../controllers/loginUser')
let authorize = require ('../helpers/authorization')
let transactionController = require ('../controllers/transaction')

router.post ('/register', userController.createUser)

router.put ('/updateuser', userController.updateUser)

router.post('/generatetoken', loginController.generateToken)

router.get ('/', authorize, loginController.currentUser)

router.post ('/newtransaction', authorize, transactionController.createTransaction)

router.get ('/transactions/:fullName', userController.trxbyUser)

module.exports = router 